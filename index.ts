import express,{ Express, Request, Response} from "express";
import dotenv from "dotenv";

//Configuration
dotenv.config();

//Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first Route of APP
app.get('/hello', (req: Request, res: Response) =>{
    //Send Hello World
    res.send('Welcome to GET Route: Hello! ');
})

//Execute APP Listen Request to PORT
app.listen(port, () =>{
    console.log(`EXPRESS RUNNING: Running at http://localhost:${port}`);
})